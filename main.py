# Importing of all used libraries and add-ons
from kivymd.app import MDApp
from kivymd.uix.list import TwoLineListItem, OneLineListItem
from kivy.core.window import Window
from kivymd.uix.button import MDRectangleFlatButton
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivymd.uix.picker import MDDatePicker
from kivy.properties import ObjectProperty
from kivymd.uix.picker import MDThemePicker
from os import listdir
from os.path import isfile, join
from cryptography.fernet import Fernet
from os import path
from kivymd.uix.dialog import MDDialog
from datetime import datetime
import os


# Declaring login screen
class LoginScreen(Screen):
    pass


# Declaring list screen
class ListScreen(Screen):
    pass


# Declaring assignment screen
class AssignmentScreen(Screen):
    pass


# Creating custom dialog for updates
class Due(MDDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        # Setting update list working ID
        self.updateList = self.ids.updateList
        # Load all files from user's folder
        files = [f for f in listdir(user_directory) if isfile(join(user_directory, f))]
        for file in files:
            with open(join(user_directory, file), 'r') as f:
                # Remove new line from deadline
                temp = f.readline().strip()
                # Correct format to perform maths
                due_date = datetime.strptime(temp.replace("-", ""), "%Y%m%d")
                days_due = due_date - datetime.now()
                # Calculate how many weeks until deadline
                weeks_due = days_due.days / 7
                if weeks_due < 2:
                    # If less than 2 weeks until deadline add assignment to update
                    self.updateList.add_widget(
                        OneLineListItem(text=file)
                    )


class Recommended(MDDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        # Setting recommended list working ID
        self.recommendedList = self.ids.recommendedList
        # Load all files from user's folder
        files = [f for f in listdir(user_directory) if isfile(join(user_directory, f))]
        weekly_hours = []
        i = 0
        for file in files:
            with open(join(user_directory, file), 'r') as f:
                # Add recommended weekly hours stored since assignment creation to list
                weekly_hours.append(f.readlines()[4])
                self.recommendedList.add_widget(
                    # Add assignment and weekly hours to recommended update
                    TwoLineListItem(text=file, secondary_text=weekly_hours[i] + " hours")
                )
                i += 1


# To load key from file in main directory for encryption
def load_key():
    return open("key.key", "rb").read()


# Main class of application
class AssignmentTracker(MDApp):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    # Called when program is launched
    def build(self):
        self.theme_cls.theme_style = "Dark"
        # Load kivy code from other file
        screen = Builder.load_file("main.kv")
        return screen

    # Declaration of variables used
    global user_directory
    chosen_date = ObjectProperty()

    # Used to create new account for user on login screen
    def create_account(self):
        # Checks to make sure information is entered first
        if self.root.ids.login_scr.ids.username.text:
            if self.root.ids.login_scr.ids.password.text:
                # Loading of key for encryption
                key = load_key()
                k = Fernet(key)

                # Addition of new username to list of usernames file
                f = open("files/usernames.txt", "r")
                lines = f.readlines()
                f = open("files/usernames.txt", "w")
                f.write(self.root.ids.login_scr.ids.username.text)
                f = open("files/usernames.txt", "a")
                f.write("\n")
                for i in range(len(lines)):
                    f.write(lines[i])

                # Addition of new password to list of passwords file
                f = open("files/passwords.txt", "r")
                lines = f.readlines()
                f = open("files/passwords.txt", "wb")
                password = self.root.ids.login_scr.ids.password.text.encode()
                f.write(k.encrypt(password))
                f = open("files/passwords.txt", "a")
                f.write("\n")
                for i in range(len(lines)):
                    f.write(lines[i])

                # Update login page after account is created
                self.root.ids.login_scr.ids.account.text = "Account Created"
            else:
                # Displays if password field is empty
                self.root.ids.login_scr.ids.account.text = "Enter Password"
        else:
            # Displays if username field is empty
            self.root.ids.login_scr.ids.account.text = "Enter Username"

    # Run when login button is pressed
    def login(self):
        # Loading of key for encryption
        key = load_key()
        k = Fernet(key)
        passwords = []
        # Add all encrypted passwords to a list
        with open("files/passwords.txt", "rb") as file:
            encrypted_passwords = file.readlines()
        # Decrypt all passwords
        for i in range(len(encrypted_passwords)):
            passwords.append(k.decrypt(encrypted_passwords[i]))

        # Format entered password correctly
        password = k.encrypt(self.root.ids.login_scr.ids.password.text.encode())
        input_password = k.decrypt(password)

        # Add all usernames to a list
        f = open("files/usernames.txt", "r")
        usernames = f.readlines()
        # Format all usernames correctly
        for i in range(len(usernames)):
            usernames[i] = usernames[i].strip()

        # Run through list of usernames stored
        for i in range(len(usernames)):
            # Check inputted username and password exist
            if self.root.ids.login_scr.ids.username.text == usernames[i]:
                # Check username and password are from the same account
                if input_password == passwords[i]:
                    # Change user file directory for rest of program to successful login user
                    global user_directory
                    user_directory = "files/users/" + self.root.ids.login_scr.ids.username.text + "/"
                    # If user file directory does not exist yet then create one
                    if not path.exists(user_directory):
                        os.mkdir(user_directory)

                    # Load all previously stored assignments
                    self.load_assignments()
                    # Change screen to list screen
                    self.root.current = 'list'
                    # Display updates
                    self.show_due()
                else:
                    # Display if password does not match any in file
                    self.root.ids.login_scr.ids.login.text = "Password Incorrect"
            else:
                # Display if username does not match any in file
                self.root.ids.login_scr.ids.login.text = "Username Incorrect"

    # Logout to change accounts
    def logout(self):
        # Reset login page
        self.root.ids.login_scr.ids.username.text = ""
        self.root.ids.login_scr.ids.password.text = ""
        self.root.ids.login_scr.ids.login.text = "Login"
        # Change to login screen
        self.root.current = 'login'

    # Show theme picker in assignment list
    def show_theme_picker(self):
        theme_dialog = MDThemePicker()
        theme_dialog.open()

    # Show date picker when creating new assignment
    def show_date_picker(self):
        MDDatePicker(self.set_date).open()

    # Apply selected date
    def set_date(self, date_obj):
        self.chosen_date = date_obj
        # Display chosen date from picker in new assignment screen
        self.root.ids.assignment_scr.ids.pickedDates.text = str(date_obj)

    # Show close assignments in update dialog notification
    def show_due(self):
        # Load all files from user's folder
        files = [f for f in listdir(user_directory) if isfile(join(user_directory, f))]
        # If there are files show dialog box
        if len(files) > 0:
            self.dialog = MDDialog(
                size_hint=(0.8,3),
                title="Due this week:",
                type="custom",
                # Use custom layout from declaration function
                content_cls=Due(),
                buttons=[
                    # Add buttons to close dialog
                    MDRectangleFlatButton(text="Continue", on_release=lambda _: self.show_recommended())
                ]
            )
            self.dialog.open()

    # Show recommended hours for assignments in update dialog notification
    def show_recommended(self):
        # Load all files from user's folder
        files = [f for f in listdir(user_directory) if isfile(join(user_directory, f))]
        # If there are files show dialog box
        if len(files) > 0:
            # Close previous dialog box
            self.dialog.dismiss()
            self.dialog = MDDialog(
                size_hint=(0.8, 0.8),
                title="Recommended hours this week:",
                type="custom",
                # Use custom layout from declaration function
                content_cls=Recommended(),
                buttons=[
                    # Add buttons to close dialog
                    MDRectangleFlatButton(text="Continue", on_release=lambda _: self.dialog.dismiss())
                ]
            )
            self.dialog.open()

    # Empty all input fields on assignment screen
    def clear_assignment(self):
        self.root.ids.assignment_scr.ids.title.text = ""
        self.root.ids.assignment_scr.ids.module.text = ""
        self.root.ids.assignment_scr.ids.pickedDates.text = ""
        self.root.ids.assignment_scr.ids.hours.text = ""
        self.root.ids.assignment_scr.ids.weight.text = ""
        self.root.ids.assignment_scr.ids.complete.text = "Cancel Assignment"

    # Save inputted assignment details to a file
    def save_assignment(self):
        # Format inputted date for maths
        due_date = datetime.strptime(self.root.ids.assignment_scr.ids.pickedDates.text.replace("-", ""), "%Y%m%d")
        days_due = due_date - datetime.now()
        weeks_due = days_due.days / 7
        # Calculate weekly recommended hours to save
        weekly_hours = float(self.root.ids.assignment_scr.ids.hours.text) / weeks_due
        # Create file with name of assignment titel
        f = open(user_directory + self.root.ids.assignment_scr.ids.title.text, "w")
        # Write rest of inputted information to file
        f.write(self.root.ids.assignment_scr.ids.pickedDates.text + "\n" +
                self.root.ids.assignment_scr.ids.module.text + "\n" +
                self.root.ids.assignment_scr.ids.hours.text + "\n" +
                self.root.ids.assignment_scr.ids.weight.text + "\n" +
                str(int(weekly_hours)))

    # Input and length checks done before saving a file
    def check_types(self):
        # Boolean declaration
        correct_types = True
        # Check inputted hours is a float
        try:
            isinstance(float(self.root.ids.assignment_scr.ids.hours.text), float)
            self.root.ids.assignment_scr.ids.hours.hint_text = "Estimated Hours"
        except ValueError:
            self.root.ids.assignment_scr.ids.hours.hint_text = "Estimated Hours Incorrect Input"
            correct_types = False
        # Check inputted weight is a float
        try:
            isinstance(float(self.root.ids.assignment_scr.ids.weight.text), float)
            self.root.ids.assignment_scr.ids.weight.hint_text = "Weight"
        except ValueError:
            self.root.ids.assignment_scr.ids.weight.hint_text = "Weight Incorrect Input"
            correct_types = False
        # Check inputted date has been done correctly
        if len(self.root.ids.assignment_scr.ids.pickedDates.text) < 10:
            correct_types = False
            self.root.ids.assignment_scr.ids.pickedDates.hint_text = "Deadline Incorrect Input"
        else:
            self.root.ids.assignment_scr.ids.pickedDates.hint_text = "Deadline"
        # Check the assignment has been given a title
        if len(self.root.ids.assignment_scr.ids.title.text) < 1:
            correct_types = False
            self.root.ids.assignment_scr.ids.title.hint_text = "Assignment Title Incorrect Input"
        else:
            self.root.ids.assignment_scr.ids.title.hint_text = "Assignment Title"
        # If all checks pass save assignment and load assignment list
        if correct_types:
            self.save_assignment()
            self.load_assignments()
            self.root.current = 'list'

    # Used to load all of a user's saved assignments
    def load_assignments(self):
        # Empty the displayed list
        self.root.ids.list_scr.ids.assignmentList.clear_widgets()
        deadlines = []
        # Load all files from user's folder
        files = [f for f in listdir(user_directory) if isfile(join(user_directory, f))]
        for file in files:
            # Create a list of deadlines corresponding to file name
            with open(join(user_directory, file), 'r') as f:
                deadlines.append(f.readline())
        for i in range(len(files)):
            # Create displayed list of assignments and their deadlines
            self.root.ids.list_scr.ids.assignmentList.add_widget(
                # When an assignment is selected then show it
                TwoLineListItem(text=files[i], secondary_text=deadlines[i], on_press=lambda x, item=files[i]: self.display_assignment(item))
            )

    # Used to load selected assignment
    def display_assignment(self, selection):
        print(selection)
        # Update and load assignment screen
        self.root.ids.assignment_scr.ids.complete.text = "Complete Assignment"
        self.root.current = 'assignment'
        # Open selected file from directory
        f = open(user_directory + selection)
        # Read all file information
        lines = f.readlines()
        for i in range(len(lines)):
            # Format all information correctly
            lines[i] = lines[i].strip()
        # Display information on correct lines
        self.root.ids.assignment_scr.ids.title.text = selection
        self.root.ids.assignment_scr.ids.pickedDates.text = lines[0]
        self.root.ids.assignment_scr.ids.module.text = lines[1]
        self.root.ids.assignment_scr.ids.hours.text = lines[2]
        self.root.ids.assignment_scr.ids.weight.text = lines[3]

    # Used to remove assignment from user's folder
    def complete_assignment(self):
        # Check their is an input in title
        if self.root.ids.assignment_scr.ids.title.text:
            # Check the file exists
            if path.exists(user_directory + self.root.ids.assignment_scr.ids.title.text):
                # Delete the file
                os.remove(user_directory + self.root.ids.assignment_scr.ids.title.text)
        # Reset assignment screen
        self.root.ids.assignment_scr.ids.title.text = ""
        self.root.ids.assignment_scr.ids.module.text = ""
        self.root.ids.assignment_scr.ids.pickedDates.text = ""
        self.root.ids.assignment_scr.ids.hours.text = ""
        self.root.ids.assignment_scr.ids.weight.text = ""
        # Load all other assignments into list
        self.load_assignments()
        # Change current screen to list screen
        self.root.current = 'list'

    # Used to initiate merge sort
    def sort(self):
        # Empty displayed list
        self.root.ids.list_scr.ids.assignmentList.clear_widgets()
        # Run if choosing to sort by weight
        if self.root.ids.list_scr.ids.sortButton.text == "Sorted by: Deadline":
            self.sort_weight()
            self.root.ids.list_scr.ids.sortButton.text = "Sorted by: Weighting"
        # Run if choosing to sort by deadline
        else:
            self.sort_date()
            self.root.ids.list_scr.ids.sortButton.text = "Sorted by: Deadline"

    # Correspond sorted data to displayed data
    def load_sort(self, index, deadlines):
        for i in range(len(index)):
            # Display sorted list
            self.root.ids.list_scr.ids.assignmentList.add_widget(
                TwoLineListItem(text=index[i], secondary_text=deadlines[i], on_press=lambda x, item=index[i]: self.display_assignment(item))
            )

    def sort_date(self):
        deadlines = []
        index = []
        # Load all files from user's folder
        files = [f for f in listdir(user_directory) if isfile(join(user_directory, f))]
        for file in files:
            with open(join(user_directory, file), 'r') as f:
                # Create list of deadlines
                deadlines.append(f.readline())
            # Create list of assignments
            index.append(file)
        # Run merge sort on list of deadlines and files
        self.merge_sort(deadlines, index)
        # Load sorted lists into screen
        self.load_sort(index, deadlines)

    def sort_weight(self):
        weights = []
        index = []
        i = 0
        # Load all files from user's folder
        files = [f for f in listdir(user_directory) if isfile(join(user_directory, f))]
        for file in files:
            with open(join(user_directory, file), 'r') as f:
                # Create list of weights
                weights.append(f.readlines()[3])
            # Formatting the weights list
            weights[i] = weights[i].strip()
            # Create list of assignments
            index.append(file)
            i += 1
        # Run merge sort on weights and files
        self.merge_sort(weights, index)
        # Add % to end of each sorted weight
        for i in range(len(weights)):
            weights[i] = weights[i] + "%"
        # Load sorted lists into screen
        self.load_sort(index, weights)

    # Main recursive merge sort algorithm
    def merge_sort(self, to_sort, index):
        # If list has more than one item
        if len(to_sort) > 1:
            # Establish midpoint of list
            midpoint = len(to_sort) // 2
            # Create left side of midpoint lists
            left = to_sort[:midpoint]
            left_index = index[:midpoint]
            # Create right side of midpoint lists
            right = to_sort[midpoint:]
            right_index = index[midpoint:]
            # Run merge sort recursively until only one item in each list
            self.merge_sort(left, left_index)
            self.merge_sort(right, right_index)

            i = j = k = 0
            # Check if number is supposed to be in middle
            while i < len(left) and j < len(right):
                # Put number in middle
                if left[i] < right[j]:
                    # Assigning number lower than comparison
                    to_sort[k] = left[i]
                    index[k] = left_index[i]
                    i += 1
                else:
                    # Assigning number high than comparison
                    to_sort[k] = right[j]
                    index[k] = right_index[j]
                    j += 1
                k += 1

            # Assign lower numbers left
            while i < len(left):
                to_sort[k] = left[i]
                index[k] = left_index[i]
                i += 1
                k += 1

            # Assign higher numbers right
            while j < len(right):
                to_sort[k] = right[j]
                index[k] = right_index[j]
                j += 1
                k += 1


# Declare size of application window
Window.size = (350, 700)
# Run application
AssignmentTracker().run()